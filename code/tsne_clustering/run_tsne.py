import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
# from sklearn.manifold import TSNE

tsne = True
ch_name = '4channels' # 4channels, 5channels, MTthresh
crop = False

if tsne:
    from tsnecuda import TSNE
else:
    import umap


def remove_nan_np(np_mat):
    """function to replace Na's with 0 in numpy matrix"""
    # count nan's in file
    print("number of NaNs in matrix:",np.count_nonzero(np.isnan(np_mat)))
    np.argwhere(np.isnan(np_mat))

    #convert nans to 0
    print("Converting NaNs to 0")
    where_are_NaNs = np.isnan(np_mat)
    np_mat[where_are_NaNs] = 0
    

print("starting program")

# load 4d .mat file
if ch_name == '4channels':
    f_mat = scipy.io.loadmat('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/data/emc_0075_image_matrix.mat')
    mat = f_mat['emc_0075']
    num_chan = 4

# load T2wMTthres2 file
elif ch_name == 'MTthresh':
    mt_thres = scipy.io.loadmat('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/data/emc_0075_T2w_MTthresh.mat')
    mat = mt_thres['T2w_MTthresh']
    
# create 5 channels .mat file
elif ch_name == '5channels':
    f_mat = scipy.io.loadmat('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/data/emc_0075_image_matrix.mat')
    mat4d = f_mat['emc_0075']
    mt_thres = scipy.io.loadmat('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/data/emc_0075_T2w_MTthresh.mat')
    mat_Mthresh = mt_thres['T2w_MTthresh']
    
    mat = np.zeros((380,300,300,5))
    mat[:,:,:,:4] = mat4d
    mat[:,:,:,4] = mat_Mthresh
    
    num_chan = 5

    

# crop the matrix
if crop:
    mat = mat[55:325, 45:255, 45:255]


#manage missing values
remove_nan_np(mat)
depth = mat.shape[0]
width = mat.shape[1]


# create subsets of 3d windows
for d in range(4, 30):
    if ch_name == 'MTthresh':
        w_h_d = d # 3d window size
        rect_mat = np.zeros((int(depth/w_h_d)*int(width/w_h_d)*int(width/w_h_d), w_h_d,w_h_d, w_h_d))
        count = 0
        for k in range(int(depth/w_h_d)):
            for i in range(int(width/w_h_d)):
                for j in range(int(width/w_h_d)):
                    rect_mat[count, :, :, :] = mat[k*w_h_d:(k+1)*w_h_d, i*w_h_d:(i+1)*w_h_d,j*w_h_d:(j+1)*w_h_d]
                    count += 1

        mat_reshape = rect_mat.reshape(int(depth/w_h_d)*int(width/w_h_d)*int(width/w_h_d), w_h_d*w_h_d*w_h_d )

    else:
        w_h_d = d # 3d window size
        rect_mat = np.zeros((int(depth/w_h_d)*int(width/w_h_d)*int(width/w_h_d), w_h_d,w_h_d, w_h_d, num_chan))
        count = 0
        for k in range(int(depth/w_h_d)):
            for i in range(int(width/w_h_d)):
                for j in range(int(width/w_h_d)):
                    rect_mat[count, :, :, :, :] = mat[k*w_h_d:(k+1)*w_h_d, i*w_h_d:(i+1)*w_h_d,j*w_h_d:(j+1)*w_h_d, :]
                    count += 1

        mat_reshape = rect_mat.reshape(int(depth/w_h_d)*int(width/w_h_d)*int(width/w_h_d), w_h_d*w_h_d*w_h_d * num_chan)


    data = mat_reshape

    if tsne:
        tsne = TSNE()
        embedding = tsne.fit_transform(data)
        plot_title = 'TSNE'
    else:
        reducer = umap.UMAP(low_memory=True)
        embedding = reducer.fit_transform(data)
        plot_title = 'UMAP'

    # save embeddings
    df_embedding = pd.DataFrame(embedding)
    df_embedding.to_csv('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/code/figures/{t}/embeddings/{t}_{ch}_{i}{i}{i}.csv'.format(t=plot_title, ch = ch_name, i=d))

    # plot tsne
    fig, ax = plt.subplots()
    ax.scatter(embedding[:, 0], embedding[:, 1],alpha=0.3, s=1)
    ax.set_title('{} projection of MRI {ch} {i}*{i}*{i} cube'.format(plot_title, ch = ch_name, i = d))
    plt.savefig('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/code/figures/{t}/{t}_{ch}_{i}{i}{i}.png'.format(t=plot_title, ch = ch_name, i=d))


# command to run on wexac
#bsub -q gpu-short -gpu num=1:j_exclusive=yes -J tsnecuda -oo cluster_output/tsnecuda_%J.out -eo cluster_output/tsnecuda_%J.err -R rusage[mem=8192] -R affinity[thread*10] ". /home/labs/bioservices/services/miniconda2/etc/profile.d/conda.sh;conda activate tsne_gpu;python NoteBooks/06_pregnancy_MRI/code/run_tsne.py"
