import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import umap
from sklearn.manifold import TSNE

def remove_nan_np(np_mat):
    """function to replace Na's with 0 in numpy matrix"""
    # count nan's in file
    print("number of NaNs in matrix:",np.count_nonzero(np.isnan(np_mat)))
    np.argwhere(np.isnan(np_mat))

    #convert nans to 0
    print("Converting NaNs to 0")
    where_are_NaNs = np.isnan(np_mat)
    np_mat[where_are_NaNs] = 0
    
    
# load .mat file
print("starting program")
mat = scipy.io.loadmat('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/data/emc_0075_image_matrix.mat')
mat4d = mat['emc_0075']

mat4d = mat4d[55:325, 45:255, 45:255]
# mat4d = mat4d[50:100, 50:100, 50:100, :]
# mat4d = mat4d[50:330, 50:250, 50:250, :]


mt_thres = scipy.io.loadmat('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/data/emc_0075_T2w_MTthresh.mat')
mt_thres = mt_thres['T2w_MTthresh']
mt_thres = mt_thres[55:325, 45:255, 45:255]

#reshape mtThresh
mat4d_reshape = mt_thres.reshape((int(270*210*210/(3*3*3)),3*3*3))

#manage missing values
remove_nan_np(mat4d)

# get basic statistics
print("min val:", np.nanmin(mat4d[:,:,:]))
print("max val:", np.nanmax(mat4d[:,:,:]))
print("mean val:",np.nanmean(mat4d[:,:,:]))


# mat4d_reshape = mat4d.reshape((int(270*210*210/(3*3*3)),3*3*3*4))
# mat4d_reshape = mat4d.reshape((mat4d.shape[0]*mat4d.shape[1]*mat4d.shape[2],mat4d.shape[3]))
# mat4d = mat4d[:,:,:,0]
# mat4d_reshape = mat4d.reshape((mat4d.shape[0]*mat4d.shape[1],mat4d.shape[2]))


#choose subsample
# K = 100000
# n_data = mat4d_reshape.shape[0] 
# idx = np.random.randint(n_data, size=K) 
# mat4d_reshape = mat4d_reshape[idx,:] 


# create UMAP
reducer = umap.UMAP(low_memory=True, n_neighbors=15, n_jobs=-1, init='random')
embedding = reducer.fit_transform(mat4d_reshape)


# plot umap
plt.scatter(embedding[:, 0], embedding[:, 1], s=1,alpha=0.7)

plt.title('UMAP projection of MRI scan 4d', fontsize=24)
plt.savefig('/home/labs/bioservices/eligol/NoteBooks/06_pregnancy_MRI/code/figures/umap_T2w_MTthresh_333.png')