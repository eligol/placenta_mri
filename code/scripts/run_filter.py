import scipy.io
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import mode
from scipy.ndimage import generic_filter


out_dir = '/home/labs/bioservices/eligol/01_projects/06_pregnancy_MRI/data/emc_0156_labels/label_3/'


k=7  #number of cluster to use in Kmeants
label = 3  #label from Kmeans to filter
filter_tup = (3,3)
DISCRETE_MODE = False

# create output folder 
if DISCRETE_MODE:
    filter_dir = os.path.join(out_dir,'label_discrete_filter33')
else:
    filter_dir = os.path.join(out_dir,'label_soft_filter33')
    
if not os.path.isdir(filter_dir):
    os.mkdir(filter_dir)



def mode_val(rec):
    """helper function to filter matrix.
    return the values with most occurences in rec"""
    return mode(rec, axis=None)[0]


def make_MTthresh(mat4d):
    """function to create MTthres channel by Talia Harriss' instructions"""
    mtmask = 1 - (mat4d[:,:,:,2]/mat4d[:,:,:,0])
    mtmask[mtmask>0.3] = 1
    mtmask[mtmask<0.3] = 0

    T2w_MTthresh = mtmask * mat4d[:,:,:,3]

    return T2w_MTthresh


# clean Na
def first_clean(np_mat):
    """convert missing values to 0"""
    # count nan's in file
    print("number of NaNs in matrix:",np.count_nonzero(np.isnan(np_mat)))
    np.argwhere(np.isnan(np_mat))

    #convert nans to 0
    print("Converting NaNs to 0")
    where_are_NaNs = np.isnan(np_mat)
    np_mat[where_are_NaNs] = 0
    
    print("shape of matrix:",np_mat.shape)
    print(f"Number of Zeroes in Array --> {np_mat.size - np.count_nonzero(np_mat)}")
    print("min val:", np.nanmin(np_mat))
    print("max val:", np.nanmax(np_mat))
    print("mean val:",np.nanmean(np_mat))

    
# create MTthresh files
mat = scipy.io.loadmat('/home/labs/bioservices/eligol/01_projects/06_pregnancy_MRI/data/emc_0156_image_matrix.mat')
mat_4d = mat['plac_matrix']
    
mat_to_analyze = make_MTthresh(mat_4d)



# load the discrete labels file
all_labels_discrete = np.load('/home/labs/bioservices/eligol/01_projects/06_pregnancy_MRI/data/emc_0156_labels/labels_mask.npy')


if(DISCRETE_MODE):
    #DISCRETE MODE - 
    mat_to_filter = np.array((all_labels_discrete == label) + 0, dtype='int8')
else:
    # SOFTMODE - create a single label array in soft mode
    label_x = np.array((all_labels_discrete == label) + 0, dtype='int8')
    mat_to_filter = label_x * mat_to_analyze


print("number of unique values:", len(np.unique(mat_to_filter)))
print("min val:", mat_to_filter.min())
print("max val:", mat_to_filter.max())


# run 2d filter
if len(filter_tup) == 2:
    for i in range(0,300):
        img = generic_filter(mat_to_filter[:,:,i], mode_val, filter_tup)
        plt.imsave(os.path.join(filter_dir,'mask{}.tiff'.format(i)), img)
        mat_to_filter[:,:,i] = img
    
    # save the matric
    np.save(os.path.join(filter_dir,'label3_mask.npy'), mat_to_filter)
        
#make 3D filter        
elif len(filter_tup) == 3:
    # filter 3D matrix
    slice_img = generic_filter(mat_to_filter, mode_val, filter_tup)
    # save the matrix
    np.save(os.path.join(filter_dir,'label3_mask.npy'), slice_img)

    # save the array as separte images
    for i in range(300):
        print("saved image {}".format(i))
        plt.imsave(os.path.join(filter_dir,'mask{}.tiff'.format(i)), slice_img[:,:,i])
    
    
