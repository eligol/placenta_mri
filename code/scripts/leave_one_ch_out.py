"""
This sctipt is training the data and making prediciton, each time with dropping of a single channel.
The purpose is to see if there is a channel that doesn't add any information and can be spared.
"""

import os
import numpy as np
import glob
from PIL import Image
from fcmeans import FCM
from scipy.stats import mode
import pickle

print("Starting leave one channel out code")
sname = '8_samples'  # {4_control, 4_diabetes, 8_samples}
model = 'fcm_'  # {kmeans_, fcm_}
k = 5


def mode_val(rec):
    return mode(rec, axis=None)[0]


# Make a GIF from the slices
def make_gif(pat_dir_of_images):
    # load .png files to make the git into list
    images_for_anim = glob.glob(pat_dir_of_images + '/*.png')
    print("number of files found:", len(images_for_anim))

    frames_for_anim = []
    for idx_img in range(len(images_for_anim)):
        new_frame = Image.open(pat_dir_of_images + '/mask{}.png'.format(idx_img))
        frames_for_anim.append(new_frame)

    # save as GIF
    frames_for_anim[0].save(os.path.join(pat_dir_of_images + 'mask_gif.gif'), format='GIF',
                            append_images=frames_for_anim[1:], save_all=True, duration=400, loop=0)


# load the raw data martix
cropped_matrices = np.load('/home/labs/bioservices/eligol/01_projects/06_pregnancy_MRI/data/8_samp_cropped_norm.npy')
# uncomment for debugging
# cropped_matrices = cropped_matrices[:,:5,:5,:5,:]


# load the reference model
ref_dir = '/home/labs/bioservices/eligol/01_projects/06_pregnancy_MRI/data/8_samples_6channels_labels/FCM_{}_means/'.format(
    k)
with open(os.path.join(ref_dir, "FCM_K{}_8_samples_model.pkl".format(k)), "rb") as f:
    fcm_ref = pickle.load(f)
labels_mat_ref = np.load(os.path.join(ref_dir, 'labels_mask_6ch.npy'))
centers_ref = fcm_ref.centers

# remove a different channel each loop
for ch_to_drop in range(6):
    print("Dropping channel = ", ch_to_drop)
    chnls = [0, 1, 2, 3, 4, 5]

    # remove one channel
    chnls.remove(ch_to_drop)
    mat_to_analyze = cropped_matrices[:, :, :, :, chnls]

    # reshape the 3d array to a vector
    vec_to_cluster = np.reshape(mat_to_analyze, newshape=(
        mat_to_analyze.shape[0] * mat_to_analyze.shape[1] * mat_to_analyze.shape[2] * mat_to_analyze.shape[3],
        mat_to_analyze.shape[4]))

    # fit clustering
    fcm = FCM(n_clusters=k)
    fcm.fit(vec_to_cluster)
    labels_mat = fcm.predict(vec_to_cluster)
    print("reference centers are: ")
    print(fcm_ref.centers)
    print("dropped channel fcm centers are:")
    print(fcm.centers)

    # find a mapping between labels in model and reference model
    clusters_map = {}

    deltas_dict = {}
    for ref_k in range(k):
        for model_k in range(k):
            delta = np.abs(fcm_ref.centers[ref_k, chnls] - fcm.centers[model_k, :]).sum()
            deltas_dict[(ref_k, model_k)] = delta

    avail_ref_k = [i for i in range(k)]
    while len(avail_ref_k) != 0:
        best_k_ref, best_k_model = min(deltas_dict, key=deltas_dict.get)
        if not ((best_k_ref in clusters_map.keys()) or (best_k_model in clusters_map.values())):
            clusters_map[best_k_ref] = best_k_model
            avail_ref_k.remove(best_k_ref)
        else:
            del deltas_dict[(best_k_ref, best_k_model)]

    print("Clusters mapping is:", clusters_map)

    # apply the mapping on model predictions
    print("orig_pred:", labels_mat[:20])
    labels_mat = np.vectorize(clusters_map.get)(labels_mat)
    print("pred after mapping:", labels_mat[:20])

    # reshape the vector back to 3D array
    vec_to_image = np.reshape(labels_mat, newshape=(
        mat_to_analyze.shape[0], mat_to_analyze.shape[1], mat_to_analyze.shape[2], mat_to_analyze.shape[3]))
    all_labels_discrete = np.array(vec_to_image, dtype='int8')  # convert to 'int8'. spare memmory

    # create directory
    path_to_save_files = '/home/labs/bioservices/eligol/01_projects/06_pregnancy_MRI/data/drop_channels/FCM_{}_drop_ch{}'.format(
        k, ch_to_drop)
    os.makedirs(path_to_save_files, exist_ok=True)

    # save the matrix
    np.save(os.path.join(path_to_save_files, 'labels_mask_6ch.npy'), all_labels_discrete)
