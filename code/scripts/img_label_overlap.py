"""
This script create a figure of single slice of graysacale Placenta MRI scan with one label masked created by
fuzzy c means algorithms
"""

import os
import argparse
import numpy as np
import cv2

masks_path = '/home/labs/neeman/Collaboration/Placenta_MRI_2021/data/FCM_15_labels_mask_6ch.npy'
original_path = '/home/labs/neeman/Collaboration/Placenta_MRI_2021/data/8_samples_cropped.npy'
snames = ['emc0296_6d', 'emc0302_6d', 'emc0300_6d', 'hymc0168_6d', 'emc0023_6d', 'emc0130_6d', 'emc0178_6d',
          'hymc0144_6d']


def parse_arguments(arguments=None):
    parser = argparse.ArgumentParser(
        description="""This script produces 3 elements figure to inspect by eye if there is good overlap between mask and image.""",
        epilog='''Outputs a figure with three images.'''
    )

    # positional arguments
    parser.add_argument('--output_dir',
                        default='/home/labs/neeman/Collaboration/Placenta_MRI_2021/data/img_mask_overlaps',
                        help='where to save the figure')

    # positional arguments
    parser.add_argument('--sample_num', default=2, type=int, help='which sample to use. (between 0 and 8)')

    # positional arguments
    parser.add_argument('--num_slice', default=50, type=int, help='number of slice (between 0 and 185) to show')

    # positional arguments
    parser.add_argument('--channel', default=0, type=int, help='number of channel to show (between 0 and 5)')

    # positional arguments
    parser.add_argument('--mask_label', default=9, type=int, help='number of mask label to show')

    return parser.parse_args(arguments)


def run(args):
    sample_num = args.sample_num
    num_slice = args.num_slice
    channel = args.channel
    label = args.mask_label
    sname = snames[sample_num]

    # load and prepare mask
    all_labels = np.load(masks_path)  # load masks file
    mask = all_labels[sample_num, num_slice, :, :]  # choose sample and slie
    if label == 0:  # turn all other pixels to 0 except the label pixels
        mask[mask != label] = 1
    else:
        mask[mask != label] = 0

    # convert mask to RGB image
    mask_color = np.ndarray((mask.shape[0], mask.shape[1], 3))
    mask_color[:, :, 0] = 0
    mask_color[:, :, 1] = 0
    mask_color[:, :, 2] = mask
    mask_color = np.array(mask_color * (255 / mask_color.max())).astype(int)

    # get sample
    samples_cropped = np.load(original_path)
    slice_to_show = samples_cropped[sample_num, num_slice, :, :, channel]

    # conver the slice to RGB
    slice_gray = np.ndarray((slice_to_show.shape[0], slice_to_show.shape[1], 3))
    slice_gray[:, :, 0] = slice_to_show
    slice_gray[:, :, 1] = slice_to_show
    slice_gray[:, :, 2] = slice_to_show
    slice_gray = np.array(slice_gray * (255 / slice_gray.max())).astype(int)

    # create alpha mask
    dst = cv2.addWeighted(slice_gray, 0.7, mask_color, 0.5, 0)

    # concatenate mask, original and ovelap together
    Hori = np.concatenate((mask_color, slice_gray, dst), axis=1)

    # save figure
    cv2.imwrite(os.path.join(args.output_dir,
                             'channel_{}_label_{}_slice_{}_{}_combined.png'.format(channel, label, num_slice, sname)),
                Hori)
    print('Created figure: channel_{}_label_{}_slice_{}_{}_combined.png at {}'.format(channel, label, num_slice, sname,
                                                                                      args.output_dir))


if __name__ == '__main__':
    # parse arguments
    args = parse_arguments()

    # run program
    run(args)
